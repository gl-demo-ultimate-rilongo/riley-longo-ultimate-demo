## Agenda

- Answer questions (Async Reading)
- Address items in Secure questions issue

### Week 4 Specifics
- Review vulnerability record in merge request widget
- Merge vulnerable code into production
- Examine vulnerability in vulnerability report
- Review security dashboard
- Resolve vulnerability via merge request and record status change

~meeting_agenda