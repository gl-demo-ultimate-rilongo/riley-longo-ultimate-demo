## Week 1 Goals
- [Create your project](#create-your-project)
- Update README
- Discover [WebIDE](#web-ide), [branches](#branches), [merge requests](#merge-requests), and merging

## Create Your Project
<details><summary>Do these steps before completing the exercise</summary>
There are a few ways to create a project in GitLab. If the template exists in the same group structure as your desired project location, you can create from template. You can also create from an external source such as BitBucket and GitHub. We are going to import by project export, as we want to create a GitLab project that exists as a baseline project in a separate group.

If you have not already, create your project by importing the Ultimate Demo Template.
- [ ] Navigate to the Settings of the template project and download the project export.
- [ ] Navigate to your group and select `New Project`
- [ ] Select `Import Project` and `GitLab export`
- [ ] Choose the file for the GitLab project export as your downloaded .tar.gz. 
- [ ] Provide a Project Name as `YOUR_NAME Ultimate Demo`. You will see the URL slug also update to your-name-ultimate-demo. 
- [ ] Click `Import Project`

![Screen_Shot_2023-01-12_at_10.48.23_AM](/uploads/54457409ac12c52eb8f929aadf6783dc/Screen_Shot_2023-01-12_at_10.48.23_AM.png)

</details>

## Async Work
<details><summary> Click to expand </summary>

### The GitLab Flow
![Screen_Shot_2023-01-10_at_4.34.51_PM](/uploads/e7f5238f3d4737008174c78df04272d8/Screen_Shot_2023-01-10_at_4.34.51_PM.png)

This is the recommended flow we provide to customers. By the end of this apprenticeship, we will have a running application that follows the [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html). We are primarily focusing on the [Create](https://about.gitlab.com/features/?stage=create) stage of GitLab this week, or the "Create a merge request" section of the flow, which encompasses Source Code Management.

### Source Code Management
Source Code Management (SCM by definition is: "Source code management (SCM) is used to track modifications to a source code repository." We use SCM interchangeably with the "Create" stage, as well as version control system (VCS). VCS allows for tracking changes in source code. In GitLab, we use `git` as our VCS.

### Our Competitors
Some of our top SCM competitors are:
- [GitHub](https://about.gitlab.com/competition/github/)
- [BitBucket](https://about.gitlab.com/competition/bitbucket/)

### Commits
[Commits](https://docs.gitlab.com/ee/topics/gitlab_flow.html#commit-often-and-push-frequently) mark changes to your repository, and are noted by a commit hash, which is a string of numbers and letters.

### Projects
[Projects](https://docs.gitlab.com/ee/user/project/organize_work_with_projects.html) are used interchangeably with repositories or repos. Projects have a one-to-one mapping with repos in GitLab, meaning one project has one "repo", or collection of code. A project is a single unit, which has its own members, issues, code, permissions, deployments, pipeline configuration, and analytics. 

### Merge Requests
[Merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/) are the control planes of development. To make a code change, a developer opens a merge request, which allows them to interact with the code base via the Web IDE, their own IDE, or their Terminal. From their desired development area, they make commits to the merge request to develop a functional change. A developer can also push commits directly to the repository, but this is not recommended flow as these commits are not subject to the merge request process, approvals, pipeline runs, and other checks that the code is ready to be merged.

### Branches
[Branches](https://docs.gitlab.com/ee/user/project/repository/branches/#branches) in git are separate versions of the repository. You will have your default branch, typically called "main", which is meant to show what your repository has in production or "prod." You can create feature branches to develop new features on when you want to change the contents of your main branch. Then, you can merge the feature branch into the main branch. You can also create branches off of other branches than main, as this is a highly flexible process.

Customers may ask you about "branching strategies." A branching strategy is the "[strategy that software development teams adopt when writing, merging and deploying code when using a version control system.](https://www.flagship.io/git-branching-strategies/)" There are some common branching strategies you may want to be familiar with:

#### [GitFlow](https://nvie.com/posts/a-successful-git-branching-model/) (one of the most popular strategies for customers)
- Work is done in feature branches that are merged into a branch called `develop` (known as the integration branch), and `develop` is merged into `main` and then `main` is tagged for release.
   - A git tag is a point of your repository in time. In this case, a tag is a commit in main, or a point in time in your production-ready repository. You can create a Release from a tag and add Release notes. The value of a tag is that you can revert your repo back to the point in time of the tag.

#### [Trunk-Based Development](https://www.flagship.io/glossary/trunk-based-development/)
- Commit often to a single trunk throughout the day

### Web IDE
The [Web IDE](https://about.gitlab.com/blog/2022/05/23/the-future-of-the-gitlab-web-ide/) allows for updating files and creating commits to branches. You can also edit a single file at a time not using the Web IDE. However, the Web IDE allows you to interact with multiple files at once and associate these changes with a single commit.
</details>

## Exercise: Update README

<details><summary>Click to expand</summary>
Now that you have a project, we are going to update the README file. A README provides context on the role a project plays. It is a Markdown file, and displayed on the front page of the project.

Update README and Create Commit to New Branch
- [ ] Navigate to your project.
- [ ] Add smorris@gitlab.com as a member (Project Information) 
- [ ] Select `Web IDE`.
- [ ] Navigate to the `README.md` file.
- [ ] Update the header to `Ultimate Demo - YOUR NAME`
- [ ] Update line 3 to `Welcome to my demo project!`
- [ ] Preview your Markdown change by selecting this icon: ![Screen_Shot_2023-01-10_at_5.10.45_PM](/uploads/7e27e1a63512d6c94af1a252bff03942/Screen_Shot_2023-01-10_at_5.10.45_PM.png)
- [ ] Select Source Control on the left pane: ![Screen_Shot_2023-01-10_at_5.11.57_PM](/uploads/07bde68d52d2a5d29d4d08d15788126e/Screen_Shot_2023-01-10_at_5.11.57_PM.png)
- [ ] Write your commit message: `Update README with my info`
- [ ] Select the checkmark to commit.
- [ ] When prompted, select `Yes Commit to a new branch`
- [ ] Type your branch name `YOUR-NAME-readme-update`
- [ ] Your branch has been created. If still in the Web IDE, select Create MR: ![Screen_Shot_2023-01-10_at_5.15.18_PM](/uploads/d99cd3e8687a36070f09f1b9785c5af1/Screen_Shot_2023-01-10_at_5.15.18_PM.png)

Work with Merge Request
- [ ] Add a description of the change you made.
- [ ] Assign the MR to yourself.
- [ ] Tag @sam as a reviewer.
- [ ] Leave all settings the same.
- [ ] Preview the change you made at the bottom of the merge request page under Changes.
- [ ] Select `Create merge request`.

We have not built a pipeline yet via a .gitlab-ci.yml file, so you should not see any pipeline runs. You should see a merge request with 1 file updated. You can see your commit under the Commits tab of the merge request and the shortened commit SHA associated with the changes (i.e. cf35c99aa651dcd1c4ed6a20961aa6f12e4924fb). You will be unable to approve your own MR, as the setting has been configured that the author of the MR cannot approve their own MR. We will talk about merge request settings in our live discussion.
</details>

## Next Steps
Live Discussion:
- Answer any questions
- Discuss merge request settings
- Approve the MR
- Merge the MR

### Next Up: [Verify](#3)

~content
