## Week 2 Goals
- [ ] Create/update a .gitlab-ci.yml file
- [ ] Introduce stages, jobs, rules, the predefined variable list, scheduled pipelines
- [ ] Create a review app environment to deploy site

## Async Work
<details><summary> Click to expand </summary>

### The GitLab Flow - CI
![Screen_Shot_2023-01-10_at_4.34.51_PM](/uploads/e7f5238f3d4737008174c78df04272d8/Screen_Shot_2023-01-10_at_4.34.51_PM.png)

This is the recommended flow we provide to customers. We are primarily focusing on the `CI pipeline runs` stage of GitLab this week, which encompasses the Verify stage. In GitLab, CI is synonymous with "building and testing an application" as well as "pipelines." We will dive a bit deeper in the sections below. 

### Pipelines
Pipelines in GitLab are the heart of CI/CD, or continuous integration and continuous delivery. They are essentially running instances of whatever exists in the .gitlab-ci.yml file. Pipelines hold the instructions for jobs, and jobs exist in stages of the pipeline. Stages are ordered, such as `build`, `test`, and `deploy`, but can have any names. Each stage has 0 or more jobs, and each job exists in a stage. Finally, the .gitlab-ci.yml needs at least one job to exist that is not hidden.

### The .gitlab-ci.yml file
The .gitlab-ci.yml file contains the structure and instructions of the pipeline. Typically, the first part of the file includes the list of stages, which imply the order the stages run in. Then, jobs are written out and associated with the stage they are using a `stage` keyword. This file is extremely flexible and powerful, and has many pre-defined variables that can be used to make the pipeline behave as you want it to. [Take a look at the variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) to see all that the .gitlab-ci.yml file can use!

Here is an example .gitlab-ci.yml file:
![Screen_Shot_2023-01-17_at_9.25.24_AM](/uploads/110edde8a0bf87222fe7e9f0de627f6c/Screen_Shot_2023-01-17_at_9.25.24_AM.png)

Here you can see the list of stages, `build` and `test`, as well as 3 jobs. You can tell which items are jobs since they have a stage associated with them. The `build` stage would run first, which has one job called `build-code-job`, then the test stage would follow, which has 2 jobs called `test-code-job1` and `test-code-job2`. The jobs run specific scripts indicated by the `script` keyword. In this case, they are printing statements that can be viewed in the pipeline logs after it runs, as well as executing some commands. These commands are ones you could execute locally on your Terminal without installing any packages.

This is a fairly simple pipeline configuration, but can be made complex as pipelines install packages and dependencies, contain variables, trigger pipelines in other projects, and deploy apps to environments. 

Please note that the .gitlab-ci.yml controls the pipeline configuration on the branch it exists on. So, the .gitlab-ci.yml in your project that will be on your default branch, is your "default" pipeline. It will run upon committing to your default branch. If you change the contents of the .gitlab-ci.yml file in a branch, that branch will use the updated .gitlab-ci.yml for pipelines that run upon committing to the branch. 

#### Continuous Integration (CI) vs. Continuous Deployment (CD)
By definition, CI and CD are:
- [Continuous Integration (CI)](https://aws.amazon.com/devops/continuous-integration/) is a DevOps software development practice where developers regularly merge their code changes into a central repository, after which automated builds and tests are run. Continuous integration most often refers to the build or integration stage of the software release process and entails both an automation component (e.g. a CI or build service) and a cultural component (e.g. learning to integrate frequently).

- [Continuous Deployment (CD)](https://www.sumologic.com/glossary/continuous-deployment/) is a strategy or methodology for software releases where any new code update or change made through the rigorous automated test process is deployed directly into the live production environment, where it will be visible to customers.

As you can see, the two exist in harmony together. CI refers to the build and test component, and CD refers to deploying updates to a live application with every commit. Both seek to automate manual processes (unit testing in a pipeline rather than running tests on your local machine, and deploying to your site upon committing to and merging a branch to your repository). Thus, the .gitlab-ci.yml file encapsulates both CI and CD by using different stages to accomplish the CI and the CD tasks. 

### Our Competitors
Some of our top CI competitors are:
- CircleCI
- Jenkins
- TeamCity


Some of our top CD competitors are:
 - GitHub
 - Harness

### [Scheduled Pipelines](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
You can schedule a pipeline to run at a specified cadence. This kicks off a run of whatever is in your .gitlab-ci.yml file on the target branch at the time scheduled. You cannot schedule specific jobs to run, but rather, the entire pipeline.

### [Rules](https://docs.gitlab.com/ee/ci/yaml/#rules)
You can introduce logic into pipelines by incorporating rules into the .gitlab-ci.yml file. Rules can determine if a job runs on a pipeline. For example, you may only want to run your production deployment job when the pipeline is running on `main`. Or, you may only want to run a job when a specific file is changed. Rules allow you to save time in your pipeline runs by excluding jobs that are not necessary to run on a given branch or in a given situation. 

### [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/)
Review apps allow you to preview changes to your application before it is deployed to production. This is done by deploying to a review environment. The environment is created dynamically in the merge request and showcases changes done via commits to the merge request.

</details>

## Exercise: Create The First Iteration of Your Blog

<details><summary>Click to expand</summary>
Now that you have a project with a README, we are going to begin creating the code for your project!

Add Source Code for Your Website

- [ ] [Access the snippet](https://gitlab.com/smorris-secure-app-demo1/ultimate-demo-template/-/snippets/2485347) with Week 2's code.
- [ ] Navigate to your Web IDE.
- [ ] Create a file called `.gitlab-ci.yml` with the contents of `.gitlab-ci.yml`.
- [ ] Create folder called `public`
- [ ] Navigate to `public` and create a file called `index.html` with the contents of the snippet for `index.html.`
- [ ] Create a branch called `first-blog` and commit to it with message `ci-and-front-page`
- [ ] Create a merge request and add @sam as a reviewer

Your merge request should look like this:

![Screen_Shot_2023-01-18_at_9.22.33_AM](/uploads/198a3c3190154c155b1bf071fd25e0a0/Screen_Shot_2023-01-18_at_9.22.33_AM.png)

- [ ] When the pipeline completes, select `View app` from the merge request to see the preview of your blog.

![Screen_Shot_2023-01-18_at_9.23.43_AM](/uploads/af68eeb0c9d85698dba9436c4e7a8af4/Screen_Shot_2023-01-18_at_9.23.43_AM.png)

- [ ] Take a look at the `Pipelines` tab of the merge request and view the records of each job. Notice the artifacts that exist from the review:pages job.

*Stop for now until the live discussion.*

</details>

## Next Steps
Live Discussion:
- Merge the MR
- Perform manual deployment
- Review the .gitlab-ci.yml contents if needed
   - Show CI Pipeline editor
- Create a release with notes

### Next Up: [Plan](#4)

~content
